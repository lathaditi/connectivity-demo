package com.example.demo.model;

public class RequestInfo {

    String checkin;
    String checkout;
    String city;
    String provider = "snaptravel";

    public String getCheckin() {
        return checkin;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvider() {
        return provider;
    }
}

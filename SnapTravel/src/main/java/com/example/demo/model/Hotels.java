package com.example.demo.model;

import java.util.ArrayList;
import java.util.List;

public class Hotels {

    List<Hotel> hotels = new ArrayList<Hotel>();

    public List<Hotel> getHotelsList() {
        return hotels;
    }

    public void setHotelsList(List<Hotel> hotelsList) {
        this.hotels = hotelsList;
    }
}
package com.example.demo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
public class LegacyHotels {

    @XmlElement(name = "element")
    public List<Hotel> elements = new ArrayList<Hotel>();

    public List<Hotel> getElements() {
        return elements;
    }

    public void setElements(List<Hotel> elements) {
        this.elements = elements;
    }

}
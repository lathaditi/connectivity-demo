package com.example.demo.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "hotels")
public class HotelsEntity {

    @Id
    private Integer id;
    private String hotel_name;
    private Integer num_review;
    private String address;
    private Integer num_stars;
    private String amenities;
    private String image_url;
    private Double price_snapTravel;
    private Double price_hotelscom;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHotel_name() {
        return hotel_name;
    }

    public void setHotel_name(String hotel_name) {
        this.hotel_name = hotel_name;
    }

    public Integer getNum_review() {
        return num_review;
    }

    public void setNum_review(Integer num_review) {
        this.num_review = num_review;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getNum_stars() {
        return num_stars;
    }

    public void setNum_stars(Integer num_stars) {
        this.num_stars = num_stars;
    }

    public String getAmenities() {
        return amenities;
    }

    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public Double getPrice_snapTravel() {
        return price_snapTravel;
    }

    public void setPrice_snapTravel(Double price_snapTravel) {
        this.price_snapTravel = price_snapTravel;
    }

    public Double getPrice_hotelscom() {
        return price_hotelscom;
    }

    public void setPrice_hotelscom(Double price_hotelscom) {
        this.price_hotelscom = price_hotelscom;
    }

}

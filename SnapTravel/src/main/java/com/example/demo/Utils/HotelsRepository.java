package com.example.demo.Utils;

import com.example.demo.Entity.HotelsEntity;
import org.springframework.data.repository.CrudRepository;

public interface HotelsRepository extends CrudRepository<HotelsEntity, Integer> {
}

package com.example.demo.Utils;

import com.example.demo.model.RequestInfo;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXB;
import java.io.StringWriter;

@Component
public class UtilMethods {

    public String RequestInfoToJson(RequestInfo requestInfo) {
        Gson objGson = new Gson();
        return objGson.toJson(requestInfo);
    }

    public String RequestInfoToXML(RequestInfo requestInfo) {
        String expectedXMl = "<root>\n" +
                "<checkin>"+ requestInfo.getCheckin() + "</checkin>\n" +
                "<checkout>" + requestInfo.getCheckout() + "</checkout>\n" +
                "<city>" + requestInfo.getCity() + "</city>\n" +
                "<provider>snaptravel</provider>\n" +
                "</root>";
//        requestInfo.setProvider("snaptravel");
//        StringWriter sw = new StringWriter();
//        JAXB.marshal(requestInfo, sw);
        return expectedXMl;
    }
}

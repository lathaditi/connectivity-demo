package com.example.demo.controller;


import com.example.demo.Entity.HotelsEntity;
import com.example.demo.Service.HotelsService;
import com.example.demo.Service.LegacyHotelsService;
import com.example.demo.Utils.HotelsRepository;
import com.example.demo.Utils.UtilMethods;
import com.example.demo.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@RestController
public class RestAPIController {

    private static final Logger logger = LoggerFactory.getLogger(RestAPIController.class);

    @Autowired
    private HotelsRepository hotelsRepository;

    private final UtilMethods utilMethods = new UtilMethods();

//    @Autowired
//    private RequestInfo requestInfoFromCmd;


    // A cron method to request hotels info using parameters from command line
//    @Scheduled(cron = "*/10 * * * * *")
//    public void saveSnapTravelHotels() {
//        logger.info("dsadsfasdasdadadasdads:" + requestInfoFromCmd.getCity());
//        saveSnapTravelHotels(requestInfoFromCmd.getCity(),
//                requestInfoFromCmd.getCheckin(),
//                requestInfoFromCmd.getCheckout());
//    }


    @RequestMapping(value = "/snapTravelHotels/{city}/{checkin}/{checkout}", method = RequestMethod.GET)
    public ResponseEntity<String> saveSnapTravelHotels(@PathVariable String city,
                                                       @PathVariable String checkin,
                                                       @PathVariable String checkout) {

        final RequestInfo requestInfo = new RequestInfo();

        requestInfo.setCity(city);
        requestInfo.setCheckin(checkin);
        requestInfo.setCheckout(checkout);

        try {

            int threadNum = 2;
            ExecutorService executor = Executors.newFixedThreadPool(threadNum);

            final String requestInfoJson = utilMethods.RequestInfoToJson(requestInfo);

            // Start thread for the hotels with snaptravel price
            FutureTask<List<Hotel>> futureTask_1 = new FutureTask<List<Hotel>>(new Callable<List<Hotel>>() {
                final HotelsService hotelsService = new HotelsService();
                @Override
                public List<Hotel> call() {
                    return hotelsService.getHotels(requestInfoJson);
                }
            });
            executor.execute(futureTask_1);

             final String requestInfoXML = utilMethods.RequestInfoToXML(requestInfo);

            // Start thread for legacy hotels
            FutureTask<List<Hotel>> futureTask_2 = new FutureTask<List<Hotel>>(new Callable<List<Hotel>>() {
                final LegacyHotelsService legacyHotelsService = new LegacyHotelsService();
                @Override
                public List<Hotel> call() {
                    try {
                        return legacyHotelsService.getLegacyHotels(requestInfoXML);
                    } catch (JAXBException e) {
                        throw new IllegalArgumentException();
                    }
                }
            });
            executor.execute(futureTask_2);
            executor.shutdown();

            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            List<Hotel> hotelsList = futureTask_1.get();
            List<Hotel> legacyHotels = futureTask_2.get();

            saveHotelsToDatabase(hotelsList, legacyHotels);

        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

        return ResponseEntity.ok("Hotels info saved successfully for SnapTravel");
    }

    private void saveHotelsToDatabase(List<Hotel> hotelsList, List<Hotel> legacyHotels) {

        Set<Integer> commonHotels = legacyHotels.stream().map(Hotel::getId).collect(Collectors.toSet());
        List<Hotel> hotels = hotelsList.stream()
                .filter(a -> commonHotels.contains(a.getId()))
                .collect(Collectors.toList());

        Map<Integer, Double> legacyHotelsMap = legacyHotels.stream()
                .collect(Collectors.toMap(Hotel::getId, Hotel::getPrice));

        for (Hotel hotel: hotels) {
            String amenities = hotel.getAmenities()
                    .stream().map(n -> String.valueOf(n))
                    .collect(Collectors.joining(",","[","]"));
            HotelsEntity hotelsEntity = new HotelsEntity();
            hotelsEntity.setId(hotel.getId());
            hotelsEntity.setHotel_name(hotel.getHotel_name());
            hotelsEntity.setAddress(hotel.getAddress());
            hotelsEntity.setNum_review(hotel.getNum_reviews());
            hotelsEntity.setNum_stars(hotel.getNum_stars());
            hotelsEntity.setImage_url(hotel.getImage_url());
            hotelsEntity.setAmenities(amenities);
            hotelsEntity.setPrice_snapTravel(hotel.getPrice());
            hotelsEntity.setPrice_hotelscom(legacyHotelsMap.get(hotel.getId()));

            hotelsRepository.save(hotelsEntity);
        }
    }

}

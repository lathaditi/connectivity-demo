package com.example.demo;

import com.example.demo.model.RequestInfo;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SnapTravelConnevtivityChallengeApplication {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SnapTravelConnevtivityChallengeApplication.class);
//    public static RequestInfo requestInfoFromCmd;

    public static void main(String[] args) {
        SpringApplication.run(SnapTravelConnevtivityChallengeApplication.class, args);
    }

//    @Bean
//    public static RequestInfo returnRequestInfo() {
//        return requestInfoFromCmd;
//    }

}

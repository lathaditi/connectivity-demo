package com.example.demo.Service;

import com.example.demo.model.Hotel;
import com.example.demo.model.LegacyHotels;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;


@Service
public class LegacyHotelsService {

    private static final Logger logger = LoggerFactory.getLogger(LegacyHotelsService.class);

    private final RestTemplate restTemplate;

    public LegacyHotelsService() {
        restTemplate = new RestTemplate();
    }

    public List<Hotel> getLegacyHotels(String requestInfo) throws JAXBException {

        final String uri = "https://experimentation.getsnaptravel.com/interview/legacy_hotels";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        headers.setContentType(MediaType.APPLICATION_XML);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
        HttpEntity<String> entity = new HttpEntity<String>(requestInfo, headers);

        String result = restTemplate.postForObject(uri, entity, String.class);

        LegacyHotels legacyHotels = JAXB.unmarshal(new StringReader(result), LegacyHotels.class);

        return legacyHotels.getElements();
    }

}

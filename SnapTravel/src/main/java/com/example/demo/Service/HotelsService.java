package com.example.demo.Service;


import com.example.demo.model.Hotel;
import com.example.demo.model.Hotels;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class HotelsService {
    private static final Logger logger = LoggerFactory.getLogger(HotelsService.class);

    private final RestTemplate restTemplate;

    public HotelsService() {
        restTemplate = new RestTemplate();
    }

    public List<Hotel> getHotels(String requestinfo) {
        final String uri = "https://experimentation.getsnaptravel.com/interview/hotels";

        HttpHeaders headers = new HttpHeaders();
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(requestinfo, headers);

        String result = restTemplate.postForObject(uri, entity, String.class);
        Gson objGson = new Gson();
        Hotels hotelsArr = objGson.fromJson(result, Hotels.class);

        return hotelsArr.getHotelsList();

    }

}
